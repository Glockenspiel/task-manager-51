package ru.t1.sukhorukova.tm.dto.response.project;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {
}
