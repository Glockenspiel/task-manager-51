package ru.t1.sukhorukova.tm.exception.field;

public final class NameEmptyException extends AbstractFieldExceprion {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
