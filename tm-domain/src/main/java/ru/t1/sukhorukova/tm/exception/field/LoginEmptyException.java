package ru.t1.sukhorukova.tm.exception.field;

public class LoginEmptyException extends AbstractFieldExceprion {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
