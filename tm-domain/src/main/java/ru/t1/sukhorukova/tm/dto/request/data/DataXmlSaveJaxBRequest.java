package ru.t1.sukhorukova.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataXmlSaveJaxBRequest extends AbstractUserRequest {

    public DataXmlSaveJaxBRequest(@Nullable String token) {
        super(token);
    }

}
