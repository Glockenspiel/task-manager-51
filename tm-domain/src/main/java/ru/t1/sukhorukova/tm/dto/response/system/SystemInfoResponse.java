package ru.t1.sukhorukova.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class SystemInfoResponse extends AbstractResponse {

    @Nullable
    private int availableProcessors;

    @Nullable
    private String freeMemoryFormat;

    @Nullable
    private String maxMemoryValue;

    @Nullable
    private String totalMemoryFormat;

    @Nullable
    private String usageMemoryFormat;

}
